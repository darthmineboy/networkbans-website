<?php
include 'core.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $settings['name'] ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?php echo $settings['name'] ?></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <form class="form">
                <div class="input-group col-md-3 col-sm-6">
                    <input type="text" class="form-control" name="player" placeholder="Player name"
                        <?php
                        if (array_key_exists('player', $_GET)) {
                            echo 'value="';
                            echo htmlspecialchars(trim($_GET['player']));
                            echo '"';
                        }
                        ?>>
                    <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                </div>
            </form>

            <table class="table table-striped margin-top">
                <tr>
                    <th>#</th>
                    <th>Player</th>
                    <th>Type</th>
                    <th>Moderator</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
                <?php
                $resultsPerPage = $settings['results_per_page'];
                $currentPage = array_key_exists('page', $_GET) ? $_GET['page'] : 1;
                if ($currentPage < 1) {
                    $currentPage = 1;
                }

                $player = array_key_exists('player', $_GET) ? $_GET['player'] : null;
                $punishments = $punishmentService->getPunishments($player, ($currentPage - 1) * $resultsPerPage, $resultsPerPage);

                foreach ($punishments['data'] as $punishment) {

                    $by_uuid = !array_key_exists('avatar_by_uuid', $settings) || $settings['avatar_by_uuid'];

                    $users = [];
                    if ($punishment->punishmentType === 'IP_BAN' || $punishment->punishmentType === 'IP_MUTE') {
                        $users = $userService->getUsersByIp($punishment->target);
                        error_log("Users with ip: " . count($users) . ' ' . $punishment->target);
                    } else {
                        $users[] = $punishment->getUser();
                    }

                    $playerColumn = implode("",array_map(
                        function ($k, $user) use ($by_uuid) {
                            $imgSrc = $by_uuid ? 'https://minotar.net/avatar/' . $user->uuid
                                : 'https://minotar.net/avatar/' . $user->name;
                            return '<div><img class="user-avatar-small" src="' . $imgSrc . '" alt = "Player Head"> ' . htmlspecialchars($user->name) . '</div>';
                        },
                        array_keys($users),
                        $users
                    ));
                    echo '
<tr>
    <td>' . $punishment->punishmentId . '</td>
    <td class="player">
        ' . $playerColumn . '
    </td>
    <td>' . $punishment->punishmentType . '</td>
    <td>' . htmlspecialchars($punishment->getIssuedByName()) . '</td>
    <td>' . htmlspecialchars($punishment->reason) . '</td>
    <td>' . $punishment->getStatus() . '</td>
</tr>';
                }
                ?>
            </table>
            <nav aria-label="Page navigation" class="pull-left">
                <ul class="pagination pagination-sm">
                    <?php
                    $maxPages = floor($punishments['results'] / $resultsPerPage);
                    if (($punishments['results'] % $resultsPerPage) != 0 || $maxPages < 1) {
                        $maxPages++;
                    }
                    $pageItems = [];
                    $startPage = $currentPage - 4;
                    if ($maxPages - $currentPage < 5) {
                        $startPage -= 5 - ($maxPages - $currentPage);
                    }
                    for ($page = $startPage; count($pageItems) < 9 && $page <= $maxPages; $page++) {
                        if ($page > 0) {
                            $pageItems[] = $page;
                        }
                    }

                    $previousPage = $currentPage - 1;
                    $enabled = $previousPage > 0;
                    ?>
                    <li <?php if (!$enabled) {
                        echo 'class="disabled"';
                    } ?>>
                        <a <?php if ($enabled) {
                            echo "href='?page={$previousPage}&player={$player}'";
                        } ?> aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <?php

                    foreach ($pageItems as $page) {
                        $class = $page == $currentPage ? 'active' : null;
                        echo "<li class=\"{$class}\"><a href=\"?page={$page}&player={$player}\">{$page}</a></li>";
                    }

                    $nextPage = $currentPage + 1;
                    $enabled = $nextPage <= $maxPages;
                    ?>
                    <li <?php if (!$enabled) {
                        echo 'class="disabled"';
                    } ?>>
                        <a <?php if ($enabled) {
                            echo "href='?page={$nextPage}&player={$player}'";
                        } ?> aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <span>Showing <b><?php echo count($punishments['data']) ?></b> of
                    <b><?php echo $punishments['results'] ?></b>
                    results
                </span>
            <div class="clearfix"></div>
            <div id="updateResponse"></div>
        </div>
    </div>
</div>
<input id="websiteVersion" type="hidden" value="<?php echo $version ?>">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="app.js"></script>
</body>
</html>