(function () {
    'use strict';

    function setUpdateResponse(response) {
        $('#updateResponse').html(response);
    }

    function checkForUpdates() {
        const URL = 'https://api.networkcore.org/resources/4';
        $.get(URL, function (data) {
            if (!data || !data.latestVersion) {
                setUpdateResponse(`<div class="alert alert-warning">An error occurred while checking for updates: Invalid response from server`);
            } else {
                const currentVersion = $('#websiteVersion').val()
                    , latestVersion = data.latestVersion;
                if (currentVersion !== latestVersion) {
                    setUpdateResponse(`<div class="alert alert-info">An update is available (current v${currentVersion}, latest v${latestVersion}). Please notify an administrator</div>`);
                }
            }
        }).fail(function () {
            setUpdateResponse(`<div class="alert alert-warning">An error occurred while checking for updates`);
        });
    }

    checkForUpdates();
})();