<?php

class Util
{
    public static function formatSeconds($seconds)
    {
        $days = floor($seconds / 86400);
        $seconds -= $days * 86400;
        $hours = floor($seconds / 3600);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $seconds -= $minutes * 60;
        if ($days != 0) {
            return "{$days} days, {$hours} hours";
        } else if ($hours != 0) {
            return "{$hours} hours, {$minutes} minutes";
        } else if ($minutes != 0) {
            return "{$minutes} minutes, {$seconds} seconds";
        } else {
            return "{$seconds} seconds";
        }
    }
}