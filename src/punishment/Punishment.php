<?php

namespace punishment;

class Punishment
{
    public $punishmentId;
    public $punishmentType;
    public $userId;
    public $target;
    public $serverId;
    public $pluginId;
    public $issuedBy;
    public $issueDate;
    public $reason;
    public $length;
    public $beginDate;
    public $expirationDate;
    public $seen;
    public $expiredBy;
    public $expiredReason;
    public $active;

    function getUser()
    {
        global $userService;
        return $userService->getUserById($this->userId);
    }

    function getIssuedBy()
    {
        global $userService;
        return $this->issuedBy == null ? null : $userService->getUserById($this->issuedBy);
    }

    function getIssuedByName()
    {
        $issuedBy = $this->getIssuedBy();
        return $issuedBy == null ? null : $issuedBy->name;
    }

    function getExpiredBy()
    {
        global $userService;
        return $this->expiredBy == null ? null : $userService->getUserById($this->expiredBy);
    }

    function getStatus()
    {
        if ($this->active) {
            if ($this->isPermanent()) {
                return 'Permanent';
            } else {
                if ($this->seen) {
                    $time = \Util::formatSeconds($this->getSecondsLeft());
                    return "Expires in {$time}";
                } else {
                    $time = \Util::formatSeconds($this->getSecondsLeft());
                    return "Expires in {$time} (once seen)";
                }
            }
            return 'Active';
        } else {
            $expiredBy = $this->getExpiredBy();
            $expiredReason = $this->expiredReason == null ? null : ' for ' . $this->expiredReason;
            return $expiredBy == null ? 'Expired' : 'Expired by ' . $expiredBy->name . $expiredReason;
        }
    }

    function getSecondsLeft()
    {
        if ($this->expirationDate == null) {
            return $this->length;
        }
        return abs(strtotime($this->expirationDate)) - time();
    }

    function isPermanent()
    {
        return $this->length === null;
    }

    function fromDatabase($row)
    {
        $this->punishmentId = $row['punishment_id'];
        $this->punishmentType = $row['punishment_type'];
        $this->userId = $row['user_id'];
        $this->target = $row['target'];
        $this->serverId = $row['server_id'];
        $this->pluginId = $row['plugin_id'];
        $this->issuedBy = $row['issued_by'];
        $this->issueDate = $row['issue_date'];
        $this->reason = $row['reason'];
        $this->length = $row['length'];
        $this->beginDate = $row['begin_date'];
        $this->expirationDate = $row['expiration_date'];
        $this->seen = $row['seen'];
        $this->expiredBy = $row['expired_by'];
        $this->expiredReason = $row['expired_reason'];
        $this->active = $row['active'];
    }

}