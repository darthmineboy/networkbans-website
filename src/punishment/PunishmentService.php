<?php

namespace punishment;

class PunishmentService
{

    function getPunishments($player, $offset, $limit)
    {
        $player = trim($player);
        if ($player === '') {
            $player = null;
        }
        if ($player != null) {
            $player = $player . '%';
        }

        global $database;
        $con = $database->getConnection();
        $stmt = $con->prepare('SELECT SQL_CALC_FOUND_ROWS * FROM nbans_punishment p 
            WHERE punishment_type IN ("BAN","MUTE","WARNING","KICK","IP_BAN","IP_MUTE") 
            AND (
                (? IS NULL OR punishment_type IN ("BAN","MUTE","WARNING","KICK") AND EXISTS (
                    SELECT 1 FROM ncore_user WHERE user_id=p.user_id AND name LIKE ?
                ))
                OR 
                (? IS NULL OR punishment_type IN ("IP_BAN","IP_MUTE") AND EXISTS (
                    SELECT 1 FROM ncore_user u INNER JOIN ncore_user_connect uc ON u.connect_id=uc.connect_id WHERE uc.ip=p.target AND u.name LIKE ?
                ))  
            ) 
            ORDER BY punishment_id DESC LIMIT ?,?');
        $stmt->bind_param('ssssii', $player, $player, $player, $player, $offset, $limit);
        $stmt->execute();
        $result = $stmt->get_result();
        $list = [];
        while ($row = $result->fetch_assoc()) {
            $punishment = new Punishment();
            $punishment->fromDatabase($row);
            $list[] = $punishment;
        }
        $stmt = $con->query('SELECT FOUND_ROWS()');
        $results = $stmt->fetch_assoc()['FOUND_ROWS()'];
        return [
            'results' => $results,
            'data' => $list
        ];
    }

}