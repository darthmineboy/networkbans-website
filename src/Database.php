<?php

class Database
{

    private $con;

    function getConnection()
    {
        if (isset($this->con)) {
            return $this->con;
        }
        global $settings;
        $dbSettings = $settings['database'];
        $this->con = new \mysqli($dbSettings['host'], $dbSettings['user'], $dbSettings['password'], $dbSettings['database']);
        if ($this->con->connect_error) {
            die('Connection failed: ' . $this->con->connect_error);
        }
        return $this->con;
    }

}