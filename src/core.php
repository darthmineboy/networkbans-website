<?php

spl_autoload_register(function ($class_name) {
    $class_name = str_replace('\\', '/', $class_name);
    include $class_name . '.php';
});

$database = new Database();
/**
 * For development purposes
 */
$hidden = file_exists('settings_hidden.php');
$settings = $hidden ? include 'settings_hidden.php' : include 'settings.php';
$version = include 'version.php';
if ($hidden) {
    $realSettings = include 'settings.php';
}
$userService = new \user\UserService();
$punishmentService = new \punishment\PunishmentService();