<?php
return [
    /**
     * This name is displayed as title and in the navigation
     */
    'name' => 'NetworkBans',
    /**
     * Configure the database credentials in this section
     */
    'database' => [
        'host' => '127.0.0.1:3306',
        'database' => 'networkbans',
        'user' => 'networkbans',
        'password' => 'networkbans'
    ],
    /**
     * How many punishments to show per page
     */
    'results_per_page' => 10,
    /**
     * Whether avatars are loaded by uuid or name
     */
    'avatar_by_uuid' => true
];