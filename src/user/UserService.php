<?php

namespace user;

use punishment\Punishment;

class UserService
{

    function getUserById($userId)
    {
        global $database;
        $con = $database->getConnection();
        $stmt = $con->prepare('SELECT * FROM ncore_user WHERE user_id=?');
        $stmt->bind_param('i', $userId);
        $stmt->execute();
        $result = $stmt->get_result();
        return $this->extractUser($result->fetch_assoc());
    }

    function getUsersByIp($ip)
    {
        global $database;
        $con = $database->getConnection();
        $stmt = $con->prepare('SELECT u.* FROM ncore_user u INNER JOIN ncore_user_connect uc ON u.connect_id=uc.connect_id WHERE uc.ip=?');
        $stmt->bind_param('s', $ip);
        $stmt->execute();
        $result = $stmt->get_result();
        $list = [];
        while ($row = $result->fetch_assoc()) {
            $list[] = $this->extractUser($row);
        }
        return $list;
    }

    function extractUser($row)
    {
        if (!$row) {
            return null;
        }
        $user = new User();
        $user->fromDatabase($row);
        return $user;
    }

}