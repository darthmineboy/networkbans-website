<?php

namespace user;

class User
{

    public $userId;
    public $type;
    public $uuid;
    public $name;

    function fromDatabase($row)
    {
        $this->userId = $row['user_id'];
        $this->type = $row['type'];
        $this->uuid = $row['uuid'];
        $this->name = $row['name'];
        return $this;
    }
}